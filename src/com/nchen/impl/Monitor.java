package com.nchen.impl;

import com.nchen.Logger;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Monitor task simply try to create a connection to some where, keep posting 1K data every seconds to make sure
 * the connection is there
 */
public class Monitor {
    Random _rand = new Random(System.currentTimeMillis());

    public interface FailureHandler {
        /**
         *
         * @param startEpoch - epoch the health check starts
         * @param reason     - failure reason
         * @param t          - throwable
         */
        void handleFailure(long startEpoch, String reason, Throwable t);
    }

    private final int _size;
    private final FailureHandler _handler;
    private final CountDownLatch _latch = new CountDownLatch(1);
    private final ScheduledExecutorService _executor = Executors.newScheduledThreadPool(1);

    public Monitor(FailureHandler handler, int size) {
        _size = size;
        _handler = handler;
    }

    public void start(int interval, boolean onlyNoise) {
        _executor.scheduleWithFixedDelay(new NoiseTask(), 0, 1, TimeUnit.SECONDS);
        if(!onlyNoise) {
            _executor.scheduleWithFixedDelay(new CheckTask(), interval, interval, TimeUnit.SECONDS);
        }
    }

    public void await() throws InterruptedException {
        _latch.await();
    }

    public void shutdown() {
        _executor.shutdownNow();
    }

    class CheckTask implements Runnable {
        public void run() {
            long start = System.currentTimeMillis();
            try {
                Logger.verbose("Performing health check to www.google.com ...");

                // MonitorHttpClient.connect("www.google.com", 443);
                MonitorHttpClient.get("https://www.google.com");
                long elapse = System.currentTimeMillis() - start;

                Logger.verbose("Health check done with %d milliseconds", elapse);
            }
            catch(Exception e) {
                Logger.verbose("Health check failed: %s", e.getMessage());

                _handler.handleFailure(start, "Health check failed", e);
            }
            catch(Throwable t) {
                Logger.verbose("!!!!!!!!Unexpected health check task failure: %s", t.getMessage());
                _handler.handleFailure(start, "Handle unexpected health check task failure", t);
            }
        }
    }

    class NoiseTask implements Runnable {
        public void run() {
            if((_rand.nextInt() % 10) != 8) {
                Logger.verbose("Not perform noising task ...");
                return;
            }

            int size = _size + _rand.nextInt() % _size;

            byte[] buf = new byte[size];
            for(int i = 0; i < size; i++) {
                buf[i] = (byte)(i % 256);
            }

            try {
                Logger.verbose("Generating noise ...");

                MonitorHttpClient.post(buf);
            }
            catch(Exception e) {
                if(Logger.verbose) {
                    Logger.verbose(e, "Noise task failed: %s", e.getMessage());
                }
                else {
                    Logger.message("Noise task failed: %s", e.getMessage());
                }
            }
            catch(Throwable t) {
                if(Logger.verbose) {
                    Logger.verbose(t, "!!!!!!!!Unexpected noise task failure: %s", t.getMessage());
                }
                else {
                    Logger.message("!!!!!!!!Unexpected noise task failure: %s", t.getMessage());
                }
            }
        }
    }

}
