package com.nchen.impl;

import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 11/4/15
 * To change this template use File | Settings | File Templates.
 */
public class SocksPlainSocketFactory extends PlainConnectionSocketFactory {
    public Socket createSocket(final HttpContext context) throws IOException {
        InetSocketAddress socksaddr = (InetSocketAddress) context.getAttribute("socks.address");
        if(socksaddr != null) {
            Proxy proxy = new Proxy(Proxy.Type.SOCKS, socksaddr);
            return new Socket(proxy);
        }
        else {
            return new Socket();
        }
    }
}
