package com.nchen.impl;

import com.nchen.Logger;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 11/4/15
 * To change this template use File | Settings | File Templates.
 */
public class MonitorHttpClient {
    private static int _interval = 10;

    // private static final String _url = "https://httpbin.org/post";
    private static final String _url = "http://httpbin.org/post";
    private static CloseableHttpClient _httpclient;
    private static InetSocketAddress _proxyAddr;
    private static String _ipaddr;
    private static int _port;
    private static boolean _dynamic;

    public static void initialize(int interval) {
        if(interval < 5) {
            _interval = 5;
        }
        else {
            _interval = interval;
        }

        Registry<ConnectionSocketFactory> reg = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", new SocksPlainSocketFactory())
                .register("https", new SocksSSLSocketFactory(SSLContexts.createSystemDefault()))
                .build();

        MyHttpClientConnectionManager cm = new MyHttpClientConnectionManager(reg);

        RequestConfig config = RequestConfig.custom()
            .setSocketTimeout(_interval * 1000)
            .setConnectTimeout(_interval * 1000)
            .build();

        _httpclient = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(config).build();
    }

    public static void setProxyAddress(int port, boolean dynamic) {
        setProxyAddress("127.0.0.1", port, dynamic);
    }

    public static void setProxyAddress(String host, int port, boolean dynamic) {
        Logger.message("Using proxy address %s:%d", host, port);
        _proxyAddr = new InetSocketAddress(host, port);
        _ipaddr = host;
        _port = port;
        _dynamic = dynamic;
    }

    public static void connect(String dns, int port) throws IOException {
        Proxy p = new Proxy(Proxy.Type.SOCKS, _proxyAddr);
        Socket s = new Socket(p);
        InetSocketAddress addr = InetSocketAddress.createUnresolved(dns, port);
        long startEpoch = System.currentTimeMillis();
        try {
            s.connect(addr);
            System.out.println(String.format("Connected to %s after %d ms", s.getRemoteSocketAddress(), System.currentTimeMillis() - startEpoch));
        }
        finally {
            s.close();
        }

    }

    static HttpClientContext _createContext(String host, int port) {
        HttpClientContext context = HttpClientContext.create();
        context.setAttribute("http.host", host);
        context.setAttribute("http.port", port);

        if(_dynamic) {
            context.setAttribute("socks.address", _proxyAddr);
        }
        else {
            HttpHost proxy = new HttpHost(_ipaddr, _port, "http");
            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .setSocketTimeout(_interval * 1000)
                    .setConnectTimeout(_interval * 1000)
                    .build();

            context.setRequestConfig(config);
        }

        return context;
    }
    /**
     * Post data to _url
     *
     * @param data
     * @throws IOException
     */
    public static void post(byte[] data) throws IOException {
        post(_url, data);
    }

    public static void post(String url, byte[] data) throws IOException {
        HttpPost post = new HttpPost(url);

        HttpClientContext context = _createContext(post.getURI().getHost(), post.getURI().getPort());

        Logger.verbose("Post data to %s with %d bytes data", url, data.length);

        post.setEntity(new ByteArrayEntity(data));

        CloseableHttpResponse resp = _httpclient.execute(post, context);

        _processResponse("POST", url, resp);
    }

    public static void get(String url) throws IOException {
        HttpGet get = new HttpGet(url);

        HttpClientContext context = _createContext(get.getURI().getHost(), get.getURI().getPort());

        Logger.verbose("Get data from %s", url);

        CloseableHttpResponse resp = _httpclient.execute(get, context);

        _processResponse("GET", url, resp);
    }

    static void _processResponse(String method, String url, CloseableHttpResponse resp) throws IOException {
        try {
            if(resp.getStatusLine().getStatusCode() != 200) {
                Logger.verbose("--------------%s %s---------------\n%s", method, url, resp.getStatusLine());

                if (resp.getEntity() != null) {
                    Logger.verbose("Response size: %d", resp.getEntity().getContentLength());
                }
                else {
                    Logger.verbose("No response entity available");
                }
            }

            EntityUtils.consume(resp.getEntity());
        }
        finally {
            resp.close();
        }
    }
}
