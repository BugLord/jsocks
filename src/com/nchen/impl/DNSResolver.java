package com.nchen.impl;

import com.nchen.Logger;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.http.conn.DnsResolver;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 12/28/15
 * To change this template use File | Settings | File Templates.
 */
/**
 * Resolve DNS to Strings using dnsjava
 */
public class DNSResolver implements DnsResolver {
    private DNSResolver() {
    }

    private static final DNSResolver _INST = new DNSResolver();

    public static DNSResolver getInstance() {
        return _INST;
    }

    private static final String _DEFAULT_DNS_NAMESERVER = "8.8.8.8";

    private String _nameServer = "";
    private SimpleResolver _resolver = null;

    private ConcurrentHashMap<String, InetAddress[]> _cache = new ConcurrentHashMap<String, InetAddress[]>();

    public void initialize(String nameServer) {
        if(nameServer == null || nameServer.isEmpty()) {
            _nameServer = _DEFAULT_DNS_NAMESERVER;
        }
        else {
            _nameServer = nameServer;
        }

        try {
            _resolver = new SimpleResolver(_nameServer);

            Logger.message("Use nameserver %s for DNS resolving.", nameServer);
        }
        catch (Exception e) {
            Logger.message("Cannot use nameserver %s: %s. Use system default.", nameServer, e.getMessage());

            _nameServer = "";
            _resolver = null;
        }
    }

    @Override
    public InetAddress[] resolve(String host) throws UnknownHostException {
        try {
            return _lookup(host, "A");
            //return resolve(_nameServer, host, "A");
        }
        catch(TryAgainException te) {
            if(_cache.containsKey(host)) {
                Logger.message("Using cache due to TRY AGAIN");
                return _cache.get(host);
            }
            else {
                Logger.message("Resolve host %s failed and need try again: %s", host, te.getMessage());
                throw new UnknownHostException(String.format("Cannot resolve host %s due to exception %s", host, te.getMessage()));
            }
        }
        catch(UnknownHostException uhe) {
            throw uhe;
        }
        catch(Exception e) {
            Logger.message("Resolve host %s failed with exception:\n%s", host, Logger.exceptionToString(e));
            throw new UnknownHostException(String.format("Cannot resolve host %s due to exception %s", host, e.getMessage()));
        }
    }

    public static InetAddress resolveHost(String dns) throws Exception {
        return resolveHost(dns, "A");
    }

    public static InetAddress resolveHost(String dns, String type) throws Exception {
        if (!_INST._useDNSJava()) {
            // use system default
            Logger.message(String.format(
                    "Resolve %s using system default.", dns
            ));

            return InetAddress.getByName(dns);
        }

        // special processing, if this is an IP address, we don't need to resolve ...
        if (InetAddressValidator.getInstance().isValid(dns)) {
            // valid IP address
            Logger.message(String.format(
                    "DNS %s is a valid IP address", dns
            ));

            return InetAddress.getByName(dns);
        }

        try {
            return _INST._resolve(dns, type);
        }
        catch (Exception e) {
            // do nothing, let's try look up ...
            Logger.message(String.format(
                    "Cannot resolve %s of type %s, try lookup ...", dns, type
            ));
        }

        InetAddress []addresses =_INST._lookup(dns, type);

        return addresses[0];
    }

    protected boolean _useDNSJava() {
        return _resolver != null;
    }

    protected InetAddress[] _lookup(String dns, String type) throws Exception {
        Logger.message(String.format(
                "Lookup %s of type %s using DNSJava.", dns, type
        ));

        Lookup lookup = new Lookup(dns, Type.value(type));

        Logger.message("Start name lookup ...");

        long startTime = System.currentTimeMillis();
        lookup.setResolver(_resolver);
        lookup.run();
        long endTime = System.currentTimeMillis();

        Logger.message(String.format(
                "Lookup done after %dms.", (endTime - startTime)
        ));

        int result = lookup.getResult();
        if (result != Lookup.SUCCESSFUL) {
            if(result == Lookup.TRY_AGAIN) {
                throw new TryAgainException(lookup.getErrorString());
            }
            throw new Exception("DNSJava Lookup failed - " + lookup.getErrorString());
        }

        Record[] records = lookup.getAnswers();

        if (records == null || records.length == 0) {
            // no record found
            Logger.message(String.format(
                    "No DNS record could be found for %s with DNSJava", dns
            ));

            throw new UnknownHostException("Cannot find host - " + dns);
        }

        List<InetAddress> addresses = new ArrayList<InetAddress>();
        for(Record record : records) {
            String ip = records[0].rdataToString();
            Logger.message("Try resolving " + ip);

            Logger.message(String.format(
                    "%s of type %s resolved to %s in %d milliseconds.",
                    dns, type, ip, (endTime - startTime)
            ));


            addresses.add(InetAddress.getByName(ip));
        }

        InetAddress[] addrs = addresses.toArray(new InetAddress[0]);
        _cache.put(dns, addrs);
        return addrs;
    }

    protected static InetAddress[] resolve(String nameServer, String dns, String type) throws Exception {
        Logger.message(String.format(
                "Resolve %s of type %s using nameserver %s.",
                dns, type, nameServer
        ));

        SimpleResolver resolver = new SimpleResolver(nameServer);
        try {
            Name name = Name.fromString(dns, Name.root);
            Record rec = Record.newRecord(name, Type.value(type), DClass.IN);
            Message query = Message.newQuery(rec);
            long startTime = System.currentTimeMillis();
            Message response = resolver.send(query);
            long endTime = System.currentTimeMillis();

            boolean auth = response.getHeader().getFlag(Flags.AA) ? true : false;

            Logger.message(String.format(
                    "Lookup done after %dms. auth=%s", (endTime - startTime), auth
            ));

            Record[] records = response.getSectionArray(Section.ANSWER);
            if (records == null || records.length == 0) {
                // no record found
                throw new Exception(String.format(
                        "No DNS record found for %s on %s",
                        dns, nameServer
                ));
            }

            List<InetAddress> addresses = new ArrayList<InetAddress>();
            for (Record record : records) {
                String ip = records[0].rdataToString();
                Logger.message("Try resolving " + ip);

                Logger.message(String.format(
                        "%s of type %s resolved to %s in %d milliseconds.",
                        dns, type, ip, (endTime - startTime)
                ));


                addresses.add(InetAddress.getByName(ip));
            }

            return addresses.toArray(new InetAddress[0]);
        }
        finally {
            resolver = null;
        }
    }

    protected InetAddress _resolve(String dns, String type) throws Exception {
        Logger.message(String.format(
                "Resolve %s of type %s using nameserver %s.",
                dns, type, _nameServer
        ));

        Name name = Name.fromString(dns, Name.root);
        Record rec = Record.newRecord(name, Type.value(type), DClass.IN);
        Message query = Message.newQuery(rec);
        long startTime = System.currentTimeMillis();
        Message response = _resolver.send(query);
        long endTime = System.currentTimeMillis();

        boolean auth = response.getHeader().getFlag(Flags.AA) ? true : false;


        Record[] records = response.getSectionArray(Section.ANSWER);
        if (records == null || records.length == 0) {
            // no record found
            throw new Exception(String.format(
                    "No DNS record found for %s on %s",
                    dns, _nameServer
            ));
        }


        String ip = records[0].rdataToString();

        Logger.message(String.format(
                "%s of type %s resolved to %s in %d milliseconds. (auth=%s)",
                dns, type, ip, (endTime - startTime), auth ? "true" : "false"
        ));


        return InetAddress.getByName(ip);
    }

    protected class TryAgainException extends Exception {
        public TryAgainException(String errmsg) {
            super(errmsg);
        }
    }

    // --------------------------------------------------------------------------------
    // UT
    public static void main(String[] args) throws Exception {
        // Test <nameServer> <dns1> ... <dnsN>
        if (args.length < 2) {
            System.out.println("Usage: DNSResover <nameServer> <dns1> ... <dnsN>");
            System.exit(-1);
        }

        DNSResolver.getInstance().initialize(args[0]);

        for (int i = 1; i < args.length; i++) {
            InetAddress ia = DNSResolver.resolveHost(args[i]);

            System.out.println(String.format(
                    "########Resolved %s => %s",
                    args[i], ia.getHostAddress()
            ));
        }
    }
}
