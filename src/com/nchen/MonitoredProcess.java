package com.nchen;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 5/24/16
 * To change this template use File | Settings | File Templates.
 *
 * A monitored process will listen on input stream, if the input stream is broken or receive 'kill' command, it shall
 * quit itself
 */
public class MonitoredProcess {
    private final Process _process;
    private final OutputStream _outputStream;
    public MonitoredProcess(Process process) {
        _process = process;

        _outputStream = process.getOutputStream();

        new DefaultOutputHandler("STDOUT", process.getInputStream()).start();
        new DefaultOutputHandler("STDERR", process.getErrorStream()).start();
    }

    public int waitFor() throws InterruptedException {
        return _process.waitFor();
    }

    public synchronized void kill() {
        try {
            _outputStream.write("kill\n".getBytes());
            _outputStream.flush();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try {
                _outputStream.close();
            }
            catch(Throwable t) {

            }
        }


        try {
            _process.destroy();
        }
        catch(Throwable t) {
             t.printStackTrace();
        }
    }


    /**
     * Default output handler will read output from an inputstream (stderr or stdout) and
     * save the data read in a buffer.
     *
     * Buffered data will be returned as the output after all data read.
     */
    private static class DefaultOutputHandler extends Thread {
        private final String _prefix;
        private final BufferedReader _reader;

        public DefaultOutputHandler(String prefix, InputStream stream) {
            _prefix = prefix;
            _reader = new BufferedReader(new InputStreamReader(stream));
        }

        /**
         * Read the data from input stream asynchronously
         */
        @Override
        public void run() {
            try {
                String s;
                while ((s = _reader.readLine()) != null) {
                    System.out.println(_prefix + ": " + s);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
