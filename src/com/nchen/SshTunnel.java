package com.nchen;

import com.nchen.impl.DNSResolver;
import com.nchen.impl.Monitor;
import com.nchen.impl.MonitorHttpClient;
import com.sshtools.net.ForwardingClient;
import com.sshtools.net.SocketTransport;
import com.sshtools.publickey.InvalidPassphraseException;
import com.sshtools.publickey.SshPrivateKeyFile;
import com.sshtools.publickey.SshPrivateKeyFileFactory;
import com.sshtools.ssh.HostKeyVerification;
import com.sshtools.ssh.PublicKeyAuthentication;
import com.sshtools.ssh.SshAuthentication;
import com.sshtools.ssh.SshClient;
import com.sshtools.ssh.SshConnector;
import com.sshtools.ssh.SshException;
import com.sshtools.ssh.components.SshKeyPair;
import com.sshtools.ssh.components.SshPublicKey;
import com.sshtools.ssh2.Ssh2Context;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import socks.ProxyServer;
import socks.server.ServerAuthenticatorNone;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NoRouteToHostException;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class SshTunnel implements Monitor.FailureHandler {
    private final int _port;
    private final String _user;
    private final String _host;
    private final String _pkPath;
    private final int _size;
    private final int _interval;
    private volatile String _passphrase = "";

    // SSH client
    private volatile SshClient _sshClient = null;

    // SOCKS proxy server
    private volatile ProxyServer _proxy = null;
    private volatile ForwardingClient _client = null;

    // monitor thread to restart the tunnel
    private volatile Monitor _monitor = null;

    // if the proxy server is failing due to SSH issue
    private AtomicBoolean _starting = new AtomicBoolean(false);
    private long _lastStartEpoch = 0L;

    private final ExecutorService _executor = Executors.newSingleThreadExecutor();
    private final InetAddress _localAddr;
    private final SshConnector _connector;
    private final Ssh2Context _context;
    private volatile RestartProxyTask _restartTask = null;

    public SshTunnel(InetAddress address, int port, String host, String user, String pkPath, int size, int interval) throws UnknownHostException, SshException {
        this(address, port, host, user, pkPath, null, size, interval);
    }

    public SshTunnel(InetAddress address, int port, String host, String user, String pkPath, String pass, int size, int interval) throws UnknownHostException, SshException {
        Logger.message("Try creating SSH tunnel object  ...");
        _port = port;
        _host = host;
        _user = user;
        _pkPath = pkPath;
        _passphrase = pass;
        _size = size;
        _interval = interval;

        // _localAddr = address;
        _localAddr = Inet4Address.getLocalHost();

        /**
         * Create an SshConnector instance
         */
        Logger.message("Try creating SSH connector ...");
        _connector = SshConnector.createInstance();

        Logger.message("Try creating SSH context ...");
        _context = new Ssh2Context();

        Logger.message("Try setting socket timeouts ...");
        _context.setSocketTimeout(interval * 1000);

        // con.setSupportedVersions(1);
        // Lets do some host key verification
        HostKeyVerification hkv = new HostKeyVerification() {
            public boolean verifyHost(String hostname, SshPublicKey key) {
                Logger.message("Always trust host - " + hostname);
                try {
                    Logger.verbose("The connected host's key (%s) is %s.", key.getAlgorithm(), key.getFingerprint());
                }
                catch (SshException e) {
                }
                return true;
            }
        };

        Logger.message("Try setting host key verifier ...");
        _connector.getContext().setHostKeyVerification(hkv);

        Logger.message("SSH tunnel object created ...");
    }

    @Override
    public void handleFailure(long startEpoch, String message, Throwable t) {
        if ((startEpoch - _lastStartEpoch) < (_interval * 1000)) {
            Logger.message(t, "Detecting failure after %d ms but last restart too small. Error [%s]: %s", System.currentTimeMillis() - startEpoch, message, (t == null ? "No exception" : t.getMessage()));
            return;
        }

        if (!_starting.compareAndSet(false, true)) {
            Logger.message(t, "Detecting consecutive failure after %dms. Error [%s]: %s", System.currentTimeMillis() - startEpoch, message, (t == null ? "No exception" : t.getMessage()));
            return;
        }

        Logger.message(t, "Detecting failure after %dms. Error [%s]: %s", System.currentTimeMillis() - startEpoch, message, (t == null ? "No exception" : t.getMessage()));

        // instead of restart, let's simply quit to rely on parent to restart the proxy
        System.exit(55);
        /*
        _restartTask = new RestartProxyTask();
        _executor.execute(_restartTask);
        */
    }

    /**
     * Start in SOCKS proxy mode
     * @param dynamic
     * @throws IOException
     * @throws SshException
     * @throws InvalidPassphraseException
     */
    public void start(boolean dynamic) throws IOException, SshException, InvalidPassphraseException {
        if(dynamic && _proxy != null) {
            throw new IllegalStateException("Dynamic port forwarding already started");
        }
        else if(!dynamic && _client != null) {
            throw new IllegalStateException("Port forwarding already started");
        }

        MonitorHttpClient.setProxyAddress(_localAddr.getHostAddress(), _port, dynamic);

        _sshClient = _create();

        if(dynamic) {
            _proxy = new ProxyServer(new ServerAuthenticatorNone(), _sshClient);
            new Thread(new ProxyServerThread()).start();

            // let's start the monitor thread
            _monitor = new Monitor(this, _size);
            _monitor.start(_interval, false);

            Logger.message("Proxy started at 127.0.0.1:%d with health check interval %ds", _port, _interval);
        }
        else {
            _client = new ForwardingClient(_sshClient);
            _client.startLocalForwarding(_localAddr.getHostAddress(), _port, "127.0.0.1", _port);

            // let's start the monitor thread
            _monitor = new Monitor(this, _size);
            _monitor.start(_interval, false);

            Logger.message("Port forwarding started at 127.0.0.1:%d with health check interval %ds", _port, _interval);
        }
    }

    /**
     * Stop port forwarding
     */
    public void stop() {
        _stop(_sshClient);
        _sshClient = null;

        if(_proxy != null) {
            ProxyServer proxy = _proxy;
            _proxy = null;

            Logger.message("Shutting down proxy at 127.0.0.1:%d", _port);
            try {
                proxy.stop();
            }
            catch (Throwable t) {
            }
        }
        else if(_client != null) {
            ForwardingClient client = _client;
            _client = null;

            Logger.message("Shutting down proxy at 127.0.0.1:%d", _port);
            try {
                client.stopAllLocalForwarding();
            }
            catch(Throwable t) {

            }
        }

        Monitor monitor = _monitor;
        if (monitor != null) {
            monitor.shutdown();
        }
    }

    public void await() throws InterruptedException {
        if (_monitor != null) {
            try {
                _monitor.await();
            }
            catch (InterruptedException ie) {
                throw ie;
            }
        }
    }

    /**
     * Stop the ssh port forwarding process if possible
     */
    void _stop(SshClient client) {
        if (client != null) {
            Logger.message("Shutting down old SSH tunnel to %s@%s", _user, _host);
            // _failing = true;
            // _proxy.replace(null);

            try {
                client.disconnect();
            }
            catch (Throwable t) {
                // ignore
            }
        }
    }

    /**
     * Start a new port forwarding process ...
     *
     * @throws IOException
     */
    SshClient _create() throws IOException, SshException, InvalidPassphraseException {
        /**
         * Connect to the host
         */
        Logger.message("Try Connecting to %s@%s ...", _user, _host);
        SocketTransport transport = new SocketTransport(_host, 22);
        final SshClient ssh = _connector.connect(transport, _user, _context);

        /**
         * Authenticate the user using password authentication
         */
        int retries = 2;
        PublicKeyAuthentication pk = new PublicKeyAuthentication();
        do {
            if(retries -- <= 0) {
                Logger.message("Retries maximum retry ... giving up ...");
                return null;
            }

            SshPrivateKeyFile pkfile = SshPrivateKeyFileFactory.parse(new FileInputStream(_pkPath));
            SshKeyPair pair;
            if (pkfile.isPassphraseProtected()) {
                if ((_passphrase == null) || (_passphrase.isEmpty())) {
                    Console console = System.console();

                    if (console != null) {
                        char[] passwords = console.readPassword("Entering passphrase: ");
                        _passphrase = new String(passwords);
                    }
                    else {
                        System.out.print("Passphrase: ");
                        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                        _passphrase = reader.readLine();
                    }
                }

                pair = pkfile.toKeyPair(_passphrase);
            }
            else {
                pair = pkfile.toKeyPair(null);
            }

            pk.setPrivateKey(pair.getPrivateKey());
            pk.setPublicKey(pair.getPublicKey());

            Logger.message("Try connecting to SSH server %s and authenticating with %s ...", _host, _user);
        }
        while ((ssh.authenticate(pk) != SshAuthentication.COMPLETE) && (ssh.isConnected()));

        /**
         * Start a session and do basic IO
         */
        if (!ssh.isAuthenticated()) {
            Logger.message("Invalid passphrase or username. Try again ...");
            _passphrase = "";
            ssh.disconnect();
            return null;
        }

        Logger.message("Successfully connected to %s@%s ...", _user, _host);
        return ssh;
    }

    /**
     * RestartProxy task:
     *   to avoid interrupting current service, we always try to create new SSH client before shutting down old one
     */
    class RestartProxyTask implements Runnable {
        @Override
        public void run() {
            if (this != _restartTask) {
                Logger.message("!!!!!!!!Found unexpected RestartProxyTask!!!!!!!!");
            }

            long start = System.currentTimeMillis();
            long restarted = start;
            boolean success = false;

            try {
                SshClient old = _sshClient;

                if (_proxy != null) {
                    success = restartDyanmicProxy();
                }
                else if (_client != null) {
                    success = restartForwarding();
                }

                restarted = System.currentTimeMillis();
            }
            finally {
                if (success) {
                    Logger.message("Restart proxy task done ... (timeToRestart=%d)", restarted - start);
                }
                else {
                    Logger.message("Restart proxy task failed ... (timeToRestart=%d)", restarted - start);
                }
                _restartTask = null;
                _starting.set(false);
            }
        }

        private boolean restartForwarding() {
            Logger.message("Try starting new port forwarding ...");
            try {
                // for port forwarding, we have to create new SSH and then ...
                SshClient ssh = _create();
                if(ssh != null) {
                    Logger.message("Try stopping old proxy ...");
                    try {
                        _stop(_sshClient);
                    }
                    catch (Throwable t) {
                        Logger.message(t, "Cannot stop old proxy!");
                    }

                    _sshClient = ssh;

                    _client.stopAllLocalForwarding();

                    _client = new ForwardingClient(ssh);
                    _client.startLocalForwarding("127.0.0.1", _port, "127.0.0.1", _port);


                    _lastStartEpoch = System.currentTimeMillis();

                    return true;
                }
            }
            catch(NoRouteToHostException nrh) {
                Logger.message("No route to host exception caught!!!! Quitting");
                System.exit(44);
            }
            catch(Exception e) {
                Logger.message(e, "Cannot start new port forwarding!");
            }

            return false;
        }

        private boolean restartDyanmicProxy() {
            Logger.message("Try starting new proxy ...");
            try {
                SshClient ssh = _create();
                if (ssh != null) {
                    try {
                        _stop(_sshClient);
                    }
                    catch (Throwable t) {
                        Logger.message(t, "Cannot stop old proxy!");
                    }

                    _sshClient = ssh;

                    _proxy.replace(ssh);
                    _lastStartEpoch = System.currentTimeMillis();

                    return true;
                }
            }
            catch (Exception e) {
                Logger.message(e, "Cannot start new proxy!");
            }

            return false;
        }
    }

    class ProxyServerThread implements Runnable {
        ProxyServerThread() {
        }

        public void run() {
            _proxy.start(_port, 0, _localAddr);
        }
    }

    /**
     * Usage:
     * SshTunnel -p <port> -v <verbose> host user identity
     * @param args
     * @throws Exception
     */
    public static void main2(String[] args) throws Exception {
        /*
        Util.runClass(SshDynamicForward.class, "arg1", "arg2");

        System.exit(0);
        */

        Options options = new Options();
        options.addOption("d", "dynamic", false, "Use dynamic forwarding. Default to true");
        options.addOption("a", "address", true, "Listening IP address. Default to 127.0.0.1. Set to empty to decide the IP by system.");
        options.addOption("p", "port", true, "local listening port number, default to 9996");
        options.addOption("s", "size", true, "heartbeat package size, default to 1024");
        options.addOption("i", "interval", true, "heartbeat check interval, default to 10 second");
        options.addOption("n", "nameserver", true, "nameserver to use, default to 8.8.8.8");
        options.addOption("v", "verbose", false, "printing verbose logging on healthchecking");
        options.addOption("h", "help", false, "print help message");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args);

        if ((cmdLine.hasOption('h')) || (cmdLine.getArgList().size() != 3)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("jsocks [-p port] [-s size] [-i secs] [-d] [-v] [-h] host user identityFile", "Start a SSH tunnel as SOCKS proxy using given information", options, "", false);
            System.exit(0);
        }

        boolean dynamic = cmdLine.hasOption('d');

        if (cmdLine.hasOption('v')) {
            Logger.verbose = true;
            ProxyServer.verbose = true;
        }
        else {
            Logger.verbose = false;
            ProxyServer.verbose = false;
        }

        String address = "127.0.0.1";
        if(cmdLine.hasOption('a')) {
            address = cmdLine.getOptionValue('a');
        }

        String nameserver = "8.8.8.8";
        if(cmdLine.hasOption('n')) {
            nameserver = cmdLine.getOptionValue('n');
        }

        int port = 9996;
        if (cmdLine.hasOption('p')) {
            port = Integer.valueOf(cmdLine.getOptionValue('p'));
        }

        int size = 1024;
        if (cmdLine.hasOption('s')) {
            size = Integer.valueOf(cmdLine.getOptionValue('s'));
        }

        int interval = 10;
        if (cmdLine.hasOption('i')) {
            interval = Integer.valueOf(cmdLine.getOptionValue('i'));
        }

        String host = cmdLine.getArgList().get(0);
        String user = cmdLine.getArgList().get(1);
        String identity = cmdLine.getArgList().get(2);
        InetAddress ia = null;

        if(address.isEmpty()) {
            ia = InetAddress.getLocalHost();
        }
        else {
            ia = InetAddress.getByName(address);
        }

        Logger.message("Using nameserver %s for DNS resolution.", nameserver);

        DNSResolver.getInstance().initialize(nameserver);
        MonitorHttpClient.initialize(interval);
        Logger.message("Try create SSH tunnel to %s@%s with listening address %s:%d", user, host, ia.toString(), port);

        SshTunnel tunnel = new SshTunnel(ia, port, host, user, identity, size, interval);

        Logger.message("Starting proxy ...");
        tunnel.start(dynamic);

        Logger.message("Waiting proxy shutdown ...");
        tunnel.await();
    }

    /**
     * argument: 0         1         2      3          4      5            6            7            8               9                   10
     *           dynamic   <sshHost> <user> <identity> <pass> <listenAddr> <listenPort> <nameserver> <heartBeatSize> <heartBeatInterval> <logfile>
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) {
        try {
            boolean dynamic = "true".equalsIgnoreCase(args[0]);
            String host = args[1];
            String user = args[2];
            String identity = args[3];
            String pass = args[4];
            String address = args[5];

            int port = Integer.valueOf(args[6]);
            String nameserver = args[7];
            int interval = Integer.valueOf(args[9]);
            int size = Integer.valueOf(args[8]);

            String logfile = "";
            if(args.length > 10) {
                logfile = args[10];
            }

            if(logfile != null && !logfile.isEmpty()) {
                Logger.message("Using logfile - " + logfile);
                Logger.setLogFile(logfile);
            }

            InetAddress ia;

            if (address.isEmpty()) {
                ia = InetAddress.getLocalHost();
            }
            else {
                ia = InetAddress.getByName(address);
            }

            new MonitorThread().start();

            DNSResolver.getInstance().initialize(nameserver);
            MonitorHttpClient.initialize(interval);
            Logger.message("Try create SSH tunnel to %s@%s with listening address %s:%d", user, host, ia.toString(), port);

            SshTunnel tunnel = new SshTunnel(ia, port, host, user, identity, pass, size, interval);

            if(dynamic) {
                Logger.message("Starting SSH tunnel as proxy ...");
                tunnel.start(true);
                Logger.message("Waiting proxy shutdown ...");
            }
            else {
                Logger.message("Starting SSH tunnel as port forwarding ...");
                tunnel.start(false);
                Logger.message("Waiting port forwarding shutdown ...");
            }

            tunnel.await();

            Logger.message("SSH Tunnel terminated");
        }
        catch(Throwable t) {
            System.out.println("Proxy QUIT due to unknown exception: " + t.getMessage());
            t.printStackTrace();

            System.exit(33);
        }
    }

    static class MonitorThread extends Thread {
        public void run() {
            Logger.message("Starting monitoring thread to receive parent control ...");

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while(true) {
                try {
                    String line = br.readLine();
                    if(line == null) {
                        Logger.message("Parent closed. Quitting self");
                        System.exit(11);
                    }

                    if(line.equalsIgnoreCase("kill")) {
                        Logger.message("Thread is stopped by parent!!!");
                        System.exit(-1);
                    }

                    Logger.message("Ignore unknown command - " + line);
                }
                catch(Throwable t) {
                    // ingore
                    Logger.message(t, "Exception caught");
                    System.exit(22);
                }
            }
        }
    }
}