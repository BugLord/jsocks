package com.nchen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 4/21/16
 * To change this template use File | Settings | File Templates.
 */
public class Util {
    private Util() {}

    public static MonitoredProcess runClass(Class klass, String... args) throws IOException,
                                               InterruptedException {
        String javaHome = System.getProperty("java.home");
        String javaBin = javaHome +
                File.separator + "bin" +
                File.separator + "java";

        String classpath = System.getProperty("java.class.path");
        String className = klass.getCanonicalName();

        List<String> commands = new ArrayList<String>();

        commands.add(javaBin);
        commands.add("-cp");
        commands.add(classpath);
        commands.add(className);

        for(String arg : args) {
            commands.add(arg);
        }

        System.out.println(String.format("Try execute class %s with JAR %s and %d arguments ...", className, classpath, args.length));

        ProcessBuilder builder = new ProcessBuilder(commands.toArray(new String[0]));

        return new MonitoredProcess(builder.start());
    }
}
