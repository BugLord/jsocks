package com.nchen;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 11/5/15
 * To change this template use File | Settings | File Templates.
 */
public class Logger {
    public static boolean verbose = false;
    private static final SimpleDateFormat _dateFormatter = new SimpleDateFormat("MM-dd HH:mm:ss.SSS zzz");
    private static String _getLoggerStr(String type) {
        Thread thread = Thread.currentThread();
        return String.format("[%-6s][%s][%-16s]", type, _dateFormatter.format(new Date()), thread.getName());
    }

    private static File _logFile = null;
    private Logger() {

    }

    public static void setLogFile(String logFile) {
        _logFile = new File(logFile);
        if(_logFile.exists()) {
            try {
                System.out.println("Try move existing log file to " + logFile + ".old");
                _logFile.renameTo(new File(logFile + ".old"));
            }
            catch(Exception e) {
                // ignore
            }
        }


        if(_logFile.exists()) {
            try {
                System.out.println("Try remove existing log file " + logFile);
                _logFile.delete();
            }
            catch(Exception e) {
                // ignore
            }
        }
    }

    public static void message(String format, Object ... args) {
        message(null, format, args);
    }

    public static void message(Throwable throwable, String format, Object ... args) {
        _log("INFO", throwable, format, args);
    }

    public static void verbose(String format, Object ... args) {
        verbose(null, format, args);
    }

    public static void verbose(Throwable throwable, String format, Object ... args) {
        if(verbose) {
            _log("DEBUG", throwable, format, args);
        }
    }

    public static String exceptionToString(Throwable e) {
         String s = "";
         StringWriter sw = null;
         PrintWriter pw = null;

         try {
             sw = new StringWriter();
             pw = new PrintWriter(sw);
             e.printStackTrace(pw);
             s = s + sw.toString();
         } finally {
             try {
                 if(sw != null) {
                     sw.close();
                 }

                 if(pw != null) {
                     pw.close();
                 }
             } catch (Exception var11) {
                 ;
             }

         }

         try {
             return e.getMessage() + "\n" + s;
         } catch (Exception var12) {
             return s;
         }
     }

    private static void _log(String type, Throwable throwable, String format, Object ...args) {
        String msg = _getLoggerStr(type) + " " + String.format(format, args);
        System.out.println(msg);

        String exceptMsg = null;
        if (throwable != null) {
            exceptMsg = exceptionToString(throwable);
            System.out.println(exceptMsg);
        }

        _writeLogFile(msg, exceptMsg);
    }

    private static void _writeLogFile(String msg, String exceptMsg) {
        if(_logFile == null) {
            return;
        }

        try {
            FileWriter writer=  new FileWriter(_logFile, true);
            writer.write(msg);
            writer.write("\n");
            if(exceptMsg != null) {
                writer.write(exceptMsg);
                writer.write("\n");
            }
            writer.flush();
            writer.close();
        }
        catch(Exception e) {
            // ignore
        }
    }
}
