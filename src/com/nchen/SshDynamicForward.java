package com.nchen;

import com.nchen.impl.Monitor;
import com.nchen.impl.MonitorHttpClient;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: ning.chen
 * Date: 11/4/15
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class SshDynamicForward implements Monitor.FailureHandler {
    private final String _sshCmd;
    private final String _args;
    private final int _port;
    private final String _user;
    private final String _host;

    private volatile Process _process = null;
    private volatile Monitor _monitor = null;

    public SshDynamicForward(int port, String host, String user) {
        this("/usr/bin/ssh", "-f -C -q -N", port, host, user);
    }

    public SshDynamicForward(String ssh, String args, int port, String host, String user) {
        _sshCmd = ssh;
        _args = args;
        _port = port;
        _host = host;
        _user = user;
    }

    @Override
    public void handleFailure(long startEpoch, String message, Throwable t) {
        System.out.println("Detecting failure - " + message + ", " + (t == null ? "No exception" : t.getMessage()));

        _stop();

        try {
            _restart();
        }
        catch(Exception e) {
            System.out.println("Cannot restart service - " + e.getMessage());
        }
    }

    /**
     * Start port forwarding
     *
     * @throws IOException
     */
    public void start() throws IOException {
        if(_process != null) {
            throw new IllegalStateException("Dyanmci port forwarding already started");
        }

        MonitorHttpClient.setProxyAddress(_port, true);

        _restart();

        // let's start the monitor thread
        _monitor = new Monitor(this, 1024);

        _monitor.start(5, false);
    }

    /**
     * Stop port forwarding
     */
    public void stop() {
        _stop();

        Monitor monitor = _monitor;
        if(monitor != null) {
            monitor.shutdown();
        }
    }

    public void await() throws InterruptedException {
        if(_monitor != null) {
            try {
                _monitor.await();
            }
            catch (InterruptedException ie) {
                throw ie;
            }
        }
    }

    /**
     * Stop the ssh port forwarding process if possible
     */
    void _stop() {
        Process proc = _process;
        _process = null;

        if(proc != null) {
            try {
                proc.destroy();
            }
            catch(Throwable t) {
                // ignore
            }
        }
    }

    /**
     * Start a new port forwarding process ...
     *
     * @throws IOException
     */
    void _restart() throws IOException {
        String args = " -D " + _port + " " + _args + " ";
        if(_user != null && !_user.isEmpty()) {
            args += _user;
            args += "@";
        }

        args += _host;

        System.out.println("Will execute command - " + _sshCmd + " with args - " + args);
        ProcessBuilder pb = new ProcessBuilder(_sshCmd, args);

        _process = pb.start();
    }


    public static void main2(String[] args) throws Exception {
        SshDynamicForward forwarder = new SshDynamicForward(9996, "54.178.201.184", "ning");

        forwarder.start();

        forwarder.await();
    }

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, world with args - " + args);
        System.exit(0);
    }
}
