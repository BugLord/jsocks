package com.nchen;

import com.sshtools.publickey.SshPrivateKeyFile;
import com.sshtools.publickey.SshPrivateKeyFileFactory;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import socks.ProxyServer;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;

public class Main {
	private static volatile MonitoredProcess process = null;
	private static long restartMs = 24 * 3600 * 1000;

	public static void main(String[] args) throws Exception {
		Options options = new Options();
		options.addOption("a", "address", true, "Listening IP address. Default to 127.0.0.1. Set to empty to decide the IP by system.");
		options.addOption("p", "port", true, "local listening port number, default to 9996");
		options.addOption("s", "size", true, "heartbeat package size, default to 1024");
		options.addOption("i", "interval", true, "heartbeat check interval, default to 10 second");
		options.addOption("l", "logfile", true, "Logfile to use. Default to empty (no log file)");
		options.addOption("n", "nameserver", true, "nameserver to use, default to 8.8.8.8");
		options.addOption("r", "restart", true, "Restart hours, default to 24");
		options.addOption("d", "dynamic", false, "Use dynamic forwarding. Default to true");
		options.addOption("v", "verbose", false, "printing verbose logging on healthchecking");
		options.addOption("h", "help", false, "print help message");

		CommandLineParser parser = new DefaultParser();
		CommandLine cmdLine = parser.parse(options, args);

		if ((cmdLine.hasOption('h')) || (cmdLine.getArgList().size() != 3)) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("jsocks [-p port] [-s size] [-i secs] [-v] [-h] host user identityFile", "Start a SSH tunnel as SOCKS proxy using given information", options, "", false);
			System.exit(0);
		}

		boolean dynamic = cmdLine.hasOption('d');

		if (cmdLine.hasOption('v')) {
			Logger.verbose = true;
			ProxyServer.verbose = true;
		}
		else {
			Logger.verbose = false;
			ProxyServer.verbose = false;
		}

		String address = "127.0.0.1";
		if (cmdLine.hasOption('a')) {
			address = cmdLine.getOptionValue('a');
		}

		String nameserver = "8.8.8.8";
		if (cmdLine.hasOption('n')) {
			nameserver = cmdLine.getOptionValue('n');
		}

		int port = 9996;
		if (cmdLine.hasOption('p')) {
			port = Integer.valueOf(cmdLine.getOptionValue('p'));
		}

		int size = 1024;
		if (cmdLine.hasOption('s')) {
			size = Integer.valueOf(cmdLine.getOptionValue('s'));
		}

		int interval = 30;
		if (cmdLine.hasOption('i')) {
			interval = Integer.valueOf(cmdLine.getOptionValue('i'));
		}

		String logfile = "";
		if(cmdLine.hasOption('l')) {
			logfile = cmdLine.getOptionValue('l');
		}

		String host = cmdLine.getArgList().get(0);
		String user = cmdLine.getArgList().get(1);
		String identity = cmdLine.getArgList().get(2);
		InetAddress ia = null;

		if (address.isEmpty()) {
			ia = InetAddress.getLocalHost();
		}
		else {
			ia = InetAddress.getByName(address);
		}

		int restart = 24;
		if(cmdLine.hasOption('r')) {
			restart = Integer.valueOf(cmdLine.getOptionValue('r'));
			if(restart <= 0) {
				restart = 24;
			}
		}

		String pass = "";
		SshPrivateKeyFile pkfile = SshPrivateKeyFileFactory.parse(new FileInputStream(identity));
		if (pkfile.isPassphraseProtected()) {
			Console console = System.console();

			if (console != null) {
				char[] passwords = console.readPassword("Entering passphrase: ");
				pass = new String(passwords);
			}
			else {
				System.out.print("Passphrase: ");
				final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
				pass = reader.readLine();
			}
		}

		restartMs = restart * 3600 * 1000;

		new RestartThread().start();

		while(true) {
			/**
			 * argument: 0         1          2      3          4      5            6            7            8               9                   10
			 *           <dynamic> <sshHost>  <user> <identity> <pass> <listenAddr> <listenPort> <nameserver> <heartBeatSize> <heartBeatInterval> <logfile></logfile>
			 */
			process = Util.runClass(SshTunnel.class, dynamic ? "true" : "false", host, user, identity, pass, address, "" + port, nameserver, "" + size, "" + interval, logfile);

			int ret = process.waitFor();

			System.out.println(String.format("########Previous process quit with code %d########", ret));
		}
	}

	static class RestartThread extends Thread {
		public void run() {
			System.out.println("$$$$$$$$Restarting thread started ...");

			long lastStart = System.currentTimeMillis();

			// let's start every 24 hours
			while(true) {
				try {
					Thread.sleep(60000);
				}
				catch(InterruptedException ie) {
					// do nothing
				}

				long now = System.currentTimeMillis();
				long delta = now - lastStart;
				if(delta >= restartMs) {
					System.out.println(String.format("$$$$$$$$Restarting tunnel server as it has run %d hours ...", delta / 3600 / 1000));

					process.kill();

					System.out.println("$$$$$$$$Failed to stop process ....");

					lastStart = System.currentTimeMillis();
				}
			}
		}
	}
}

